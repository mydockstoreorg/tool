cwlVersion: v1.0
class: CommandLineTool
description: "A docker container and tool for running clustalw2. Note that when you use Dockstore to create the input parameter file, not all parameters are required. You may remove from the input parameter file any parameters which are not required, unless you would like to use them. Note that not all combinations of parameters have been tested."

dct:creator:
  foaf:name: Andrew Duncan
  foaf:mbox: "andrew.duncan@oicr.on.ca"

baseCommand: clustalw2
requirements:
  - class: DockerRequirement
    dockerPull: registry.hub.docker.com/dockstoretestuser/private_test_repo
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - $(inputs.infile)    

inputs:
  infile:
    type: File
    description: "required"
    inputBinding:
      position: 1
      prefix: -INFILE=
      separate: false
      valueFrom: $(self.basename)
  profileone:
    type: File?
    description: "optional"
    inputBinding:
      position: 2
      prefix: -PROFILE1=
      separate: false
  profiletwo:
    type: File?
    description: "optional"
    inputBinding:
      position: 3
      prefix: -PROFILE2=
      separate: false
  align:
    type: boolean
    description: "required"
    inputBinding:
      position: 4
      prefix: -ALIGN
  tree:
    type: boolean
    description: "required"
    inputBinding:
      position: 5
      prefix: -TREE
  pim:
    type: boolean
    description: "required"
    inputBinding:
      position: 6
      prefix: -PIM
  bootstrap:
    type: int?
    description: "optional"
    inputBinding:
      position: 7
      prefix: -BOOTSTRAP=
      separate: false
  convert:
    type: boolean
    description: "required"
    inputBinding:
      position: 8
      prefix: -CONVERT
  quicktree:
    type: boolean
    description: "required"
    inputBinding:
      position: 9
      prefix: -QUICKTREE
  bootstrap:
    type: string?
    description: "optional"
    inputBinding:
      position: 10
      prefix: -TYPE=
      separate: false
  negative:
    type: boolean
    description: "required"
    inputBinding:
      position: 11
      prefix: -NEGATIVE
  outfile:
    type: string?
    description: "optional"
    inputBinding:
      position: 12
      prefix: -OUTFILE=
      separate: false
  output:
    type: string?
    description: "optional"
    inputBinding:
      position: 13
      prefix: -OUTPUT=
      separate: false
  outorder:
    type: string?
    description: "optional"
    inputBinding:
      position: 14
      prefix: -OUTORDER=
      separate: false
  case:
    type: string?
    description: "optional"
    inputBinding:
      position: 15
      prefix: -CASE
  seqnos:
    type: string?
    description: "optional"
    inputBinding:
      position: 16
      prefix: -SEQNOS=
      separate: false   
  seqno_range:
    type: string?
    description: "optional"
    inputBinding:
      position: 17
      prefix: -SEQNO_RANGE=
      separate: false
  range:
    type: string?
    description: "optional"
    inputBinding:
      position: 18
      prefix: -RANGE=
      separate: false
  maxseqlen:
    type: int?
    description: "optional"
    inputBinding:
      position: 19
      prefix: -MAXSEQLEN=
      separate: false
  quiet:
    type: boolean
    description: "required"
    inputBinding:
      position: 20
      prefix: -QUIET
  stats:
    type: File?
    description: "optional"
    inputBinding:
      position: 21
      prefix: -STATS=
      separate: false
  ktuple:
    type: int?
    description: "optional"
    inputBinding:
      position: 22
      prefix: -KTUPLE=
      separate: false
  topdiags:
    type: int?
    description: "optional"
    inputBinding:
      position: 23
      prefix: -TOPDIAGS=
      separate: false
  window:
    type: int?
    description: "optional"
    inputBinding:
      position: 24
      prefix: -WINDOW=
      separate: false
  pairgap:
    type: int?
    description: "optional"
    inputBinding:
      position: 25
      prefix: -PAIRGAP=
      separate: false
  score:
    type: string?
    description: "optional"
    inputBinding:
      position: 26
      prefix: -SCORE
  pwmatrix:
    type: string?
    description: "optional"
    inputBinding:
      position: 27
      prefix: -PWMATRIX=
      separate: false
  pwdnamatrix:
    type: string?
    description: "optional"
    inputBinding:
      position: 28
      prefix: -PWDNAMATRIX=
      separate: false
  pwgapopen:
    type: int?
    description: "optional"
    inputBinding:
      position: 29
      prefix: -PWGAPOPEN=
      separate: false
  pwgapext:
    type: int?
    description: "optional"
    inputBinding:
      position: 30
      prefix: -PWGAPEXT=
      separate: false
outputs:
  alnoutput:
    type: Directory
    description: "required"
    outputBinding:
      glob: "."