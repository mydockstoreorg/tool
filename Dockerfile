FROM ubuntu:14.04
MAINTAINER Andrew Duncan <agduncan@uwaterloo.ca>

# Setup the basics
USER root
RUN apt-get -m update && apt-get install -y wget tar build-essential

# Download Clustalw
RUN wget -q http://www.clustal.org/download/current/clustalw-2.1.tar.gz
RUN tar -xvf clustalw-2.1.tar.gz && \
	rm clustalw-2.1.tar.gz && \
	cd clustalw-2.1 && \
	./configure && \
	make && \
	mv src/clustalw2 /usr/local/bin
RUN mkdir -p /home/ubuntu

# Switch back to ubuntu
RUN groupadd -r -g 1000 ubuntu && useradd -r -g ubuntu -u 1000 ubuntu
RUN chown -R ubuntu:ubuntu /home/ubuntu
USER ubuntu
WORKDIR /home/ubuntu

CMD ["/bin/bash"]
